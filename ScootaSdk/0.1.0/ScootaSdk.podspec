#
# Be sure to run `pod lib lint ScootaSdk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ScootaSdk'
  s.version          = '0.1.0'
  s.summary          = 'library to integrate with the Scoota server'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  JOIN THE WORLD'S LEADING
  RIDERS ALERT NETWORK
  BOOST YOUR SERVICE'S APPEAL WITH MINIMAL COST
                         DESC

  s.homepage         = 'https://www.scoota-app.com'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Or Azran' => 'orthehelper@gmail.com' }
  s.source           = { :git => 'https://OrAzran@bitbucket.org/scoota-app/scootapod.git', :tag => s.version.to_s }
  s.swift_version    = '4.0'
  s.ios.deployment_target = '10.0'

  s.source_files = 'ScootaSdk/Classes/**/*'
  
end
